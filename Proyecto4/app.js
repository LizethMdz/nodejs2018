'use strict'

var express = require('express'),
	favicon = require('serve-favicon'),
	bodyParser = require('body-parser'),
	//morgan = require('morgan'),
	//restFul = require('express-method-override')('_method'),
	routes = require('./routes/routes'),
	faviconURL = `${__dirname}/public/img/node-favicon.png`,
	publicDir = express.static(`${__dirname}/public`),
	viewDir = `${__dirname}/views`,
	port = (process.env.PORT || 3000),
	app = express()

app
	//configurando app
	.set('views', viewDir)
	.set('view engine', 'jade')
	//.set('view engine', 'ejs')
	.set('port', port)
	//ejecutando middlewares
	.use( favicon(faviconURL) )

	//PERMITE PARSEAR EN MODO JSON
	.use( bodyParser.json())
	.use( bodyParser.urlencoded({extended: false})) //TODA LA INFORMACION DEL FORM SE CODIFICA 
	//.use(restFul)
	//.use( morgan('dev') )
	.use(publicDir)
	//ejecuto el middleware Enrutador
	.use(routes)

module.exports = app