'use strict'


var controllers  = require('../controllers'),
	express = require('express'),
	router = express.Router()

router.get('/', controllers.HomeController.index)

	
module.exports = router