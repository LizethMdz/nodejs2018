'use strict'

var MovieModel = require('../models/movie-model'),
    MovieController = () => {}

MovieController.getAll = (req, res, next) => {
    MovieModel.getAll((err, rows) => {
        if(err)
		{
            //next( new Error('No hay registros de Peliculas'))
            let locals = {
                title : 'Error al consultar a la BD',
                description : 'Error de Sintaxis SQL',
				error : err
			}
			res.render('error', locals)
		}else{
			let locals = {
				title : 'Lista de Peliculas',
				data : rows
			}
			res.render('index', locals)
		}
    })
}

MovieController.getOneMovie = (req, res, next) => {
    let movie_id = req.params.movie_id
    console.log(movie_id)
    MovieModel.getOneMovie(movie_id, (err, rows) => {
        console.log(err, '---', rows)
        if(err)
        {
            //next( new Error('Registro No Encontrado') )
            let locals = {
                title : `Error al buscar el regostro con id : ${movie_id}`,
                description : 'Error de Sintaxis SQL',
                error : err
            }
            res.render('error', locals)
        }
        else
        {
            let locals = {
                title : 'Editar Película',
                data : rows
            }

            res.render('edit-movie', locals)
        }
    })
}

// MovieController.insert = (req, res, next) => {
//     //RECOJEMOS DATOS DEL FORMULARIO
// 			let movie = {
// 				movie_id : req.body.movie_id,
// 				name_title : req.body.name_title,
// 				release_year : req.body.release_year,
// 				rating : req.body.rating,
// 				image : req.body.image
// 			}
// 			console.log(movie);
// 			MovieModel.insert(movie, (err) =>{
//                 if(err){
//                     let locals = {
//                         title : `Error al agregar el registro a la BD con id : ${movie.movie_id}`,
//                         description : 'Error de Sintaxis SQL',
//                         error : err
//                     }
//                     res.render('error', locals)

//                 }else{
//                     res.redirect('/')
//                 }
//             })

// }

// MovieController.update = (req, res, next) => {
//     let movie = {
//         movie_id : req.body.movie_id,
//         name_title : req.body.name_title,
//         release_year : req.body.release_year,
//         rating : req.body.rating,
//         image : req.body.image
//     }
//     console.log(movie);
//     MovieModel.update(movie, (err) =>{
//         if(err){
//             let locals = {
//                 title : `Error al actualizar el registro a la BD con id : ${movie.movie_id}`,
//                 description : 'Error de Sintaxis SQL',
//                 error : err
//             }
//             res.render('error', locals)

//         }else{
//             res.redirect('/')
//         }
//     })
// }

MovieController.delete = (req, res, next) => {
    let movie_id = req.params.movie_id
    console.log(movie_id)
    MovieModel.delete(movie_id, (err, rows) => {
        console.log(err, '---', rows)
        if(err)
        {
            //next( new Error('Registro No Encontrado') )
            let locals = {
                title : `Error al borrar el registro con id : ${movie_id}`,
                description : 'Error de Sintaxis SQL',
                error : err
            }
            res.render('error', locals)
        }
        else
        {
            res.redirect('/')
        }
    })
}

MovieController.save = (req, res, next) => {
    let movie = {
        movie_id : req.body.movie_id,
        name_title : req.body.name_title,
        release_year : req.body.release_year,
        rating : req.body.rating,
        image : req.body.image
    }
    console.log(movie);
    MovieModel.save(movie, (err) =>{
        if(err){
            let locals = {
                title : `Error al salvar el registro a la BD con id : ${movie.movie_id}`,
                description : 'Error de Sintaxis SQL',
                error : err
            }
            res.render('error', locals)

        }else{
            res.redirect('/')
        }
    })
}

MovieController.addForm = (req, res, next) => {
    res.render('add-movie', { title : 'Agregar Pelicula'})
}
	

MovieController.error404 = (req, res, next) => {
    let error = new Error(),
    locals = {
        title : 'Error 404',
        description : 'Recurso No Encontrado',
        error : error
    }

    error.status = 404

    res.render('error', locals)

    next()

}


module.exports = MovieController