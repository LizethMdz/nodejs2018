//CREAR LA CONEXIÓN A LA BASE DE DATOS
'use strict'

var mysql = require('mysql'),
    conf = require('./db-conf'),
    dbOptions = {
        host :  conf.mysql.host,
        port : conf.mysql.port,
        user : conf.mysql.user,
        password : conf.mysql.password,
        database : conf.mysql.database
    },
    myConn = mysql.createConnection(dbOptions)

myConn.connect((err) =>{
    return (err) ? console.log(`Error al conectar a MySQL : ${err.stack}`) : console.log(`Conexion establecida
    con MySQL N°: ${myConn.threadId}`)
})

module.exports = myConn