'use strict'

var conn = require('./movie-connection'),
    MovieModel = () => {}

MovieModel.getAll = (cb) => {
    conn.query('SELECT * FROM movies', cb);
}

MovieModel.getOneMovie = (id, cb) => {
    conn.query('SELECT * FROM movies WHERE movie_id = ?', id, cb);
}

// MovieModel.insert = (data, cb) => {
//     conn.query('INSERT INTO movies SET ? ', data, cb);  
// }

// MovieModel.update = (data, cb) => {
//     conn.query('UPDATE movies SET ? WHERE movie_id = ?', [data, data.movie_id], cb);
// }

MovieModel.save = (data, cb) => {
    conn.query('SELECT * FROM movies WHERE movie_id = ? ', data.movie_id, (err, rows) => {
        console.log(rows.length)
        // if(rows.length == 1){
        //     conn.query('UPDATE movies SET ? WHERE movie_id = ?', [data, data.movie_id], cb);
        // }else{
        //     conn.query('INSERT INTO movies SET ? ', data, cb);  
        // }
        if(err){
            return err
        }else{
            (rows.length == 1 ) 
            ? conn.query('UPDATE movies SET ? WHERE movie_id = ?', [data, data.movie_id], cb)
            : conn.query('INSERT INTO movies SET ? ', data, cb)
        }
    })
}

MovieModel.delete = (id, cb) => {
    conn.query('DELETE FROM movies WHERE movie_id = ?', id, cb);
}

MovieModel.close = () => {

}


module.exports = MovieModel