var express = require('express');
var router = express.Router();
var controllers = require('.././controllers');
var passport = require('passport')	
var AuthMiddleware = require('.././middleware/auth');

router.get('/', controllers.HomeController.index);
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* Routes de usuario */
router.get('/auth/signup', controllers.UserController.getSignUp);
	
router.post('/auth/signup', controllers.UserController.postSignUp);
 	
router.get('/auth/signin', controllers.UserController.getSignIn);

router.post('/auth/signin', passport.authenticate('local',{
  successRedirect: '/users/panel',
  failureRedirect:'/auth/signin',
  failureFlash: true
}));




router.get('/auth/logout', controllers.UserController.logout);
router.get('/users/panel', AuthMiddleware.isLogged, controllers.UserController.getUserPanel);
router.get('/users/home', AuthMiddleware.isLogged, controllers.UserController.getPrincipal);
router.get('/users/consulta', AuthMiddleware.isLogged, controllers.UserController.getConsulta);
router.get('/users/sesiones', AuthMiddleware.isLogged, controllers.UserController.getSesion);
router.get('/users/sucursal', AuthMiddleware.isLogged, controllers.UserController.getSucursal);
router.get('/users/agentes', AuthMiddleware.isLogged, controllers.UserController.getAgente);
module.exports = router;
