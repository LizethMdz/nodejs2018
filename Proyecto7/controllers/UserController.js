var mysql = require('mysql');
var bcrypt = require('bcryptjs');

module.exports ={

    getSignUp : function(req,res,next){
        return res.render('users/signup');
    },

    postSignUp : function(req,res,next){
        //  console.log(req.body);
        //  return;
        var salt = bcrypt.genSaltSync(10);
        var password = bcrypt.hashSync(req.body.password, salt);
        var user = {
            email : req.body.email,
            password : password
        };
        var config =require('.././database/config');
        var db = mysql.createConnection(config);

        //conectamos la DB

        db.connect();

        // insertamos los valores enviados desde el formulario
        db.query('INSERT INTO usuarios SET ?', user, function(err, rows, fields){
        if(err) throw err;
            db.end();
        });
        req.flash('info', 'Se ha registrado exitosamente, Ya puedes Iniciar Sesión ADMINISTRADOR :)');

        return res.redirect('/auth/signin');
    },
    getSignIn : function(req, res, next){
        return res.render('users/signin', {message : req.flash('info'), authmessage: req.flash('authmessage')});
    },

    logout : function(req, res, next){
        req.logout();
        res.redirect('/auth/signin');
    },

    getUserPanel : function(req, res, next){
        res.render('users/panel',
        {
            isAuthenticated : req.isAuthenticated(),
            user : req.user
        });
    },

    getPrincipal : (req, res, next) => {
        var config =require('.././database/config');
        var db = mysql.createConnection(config);

        //conectamos la DB

        db.connect();

        // insertamos los valores enviados desde el formulario
        db.query('SELECT * , (SELECT regiones.Nombre from regiones WHERE regiones.idRegion = deudores.idRegion) AS Region,  (SELECT pagos.Deuda FROM pagos WHERE pagos.idDeudor = deudores.idDeudor ) AS Deudas , (SELECT pagos.FechaDeudaActual pagos FROM pagos WHERE pagos.idDeudor = deudores.idDeudor ) AS FechaD , (SELECT pagos.FechaUltimoPago pagos FROM pagos WHERE pagos.idDeudor = deudores.idDeudor ) AS FechaV FROM deudores',  (err, rows) => {

            if(err){
                console.log(err);
                // res.render('error');
            }else{
                res.render('users/home', {

                    isAuthenticated : req.isAuthenticated(),
                    user : req.user,
                    data : rows

                });

                console.log(rows);
            }

        });
},

      getSucursal : (req, res, next) => {

          var config =require('.././database/config');
          var db = mysql.createConnection(config);

            //conectamos la DB

          db.connect();

            // insertamos los valores enviados desde el formulario
          db.query('SELECT * FROM sucursales',  (err, rows) => {

              if(err){
                console.log(err);
                //   res.render('error');
              }else{
                  res.render('users/sucursal', {

                      isAuthenticated : req.isAuthenticated(),
                      user : req.user,
                      data : rows

                  });

              }

          });
},

getAgente : (req, res, next) => {

    var config =require('.././database/config');
    var db = mysql.createConnection(config);

      //conectamos la DB

    db.connect();

      // insertamos los valores enviados desde el formulario
    db.query('SELECT * FROM agentes',  (err, rows) => {

        if(err){
            console.log(err);
            // res.render('error');
        }else{
            res.render('users/agentes', {

                isAuthenticated : req.isAuthenticated(),
                user : req.user,
                data : rows

            });

        }

    });
},


    getConsulta : function(req, res, next){
        res.render('users/consulta',
        {
            isAuthenticated : req.isAuthenticated(),
            user : req.user
        });
    },

    getSesion : function(req, res, next){
        res.render('users/pagos',
        {
            isAuthenticated : req.isAuthenticated(),
            user : req.user
        });
    }

};
