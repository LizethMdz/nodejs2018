'use strict'


var movies  = require('../models/movies'),
	express = require('express'),
	router = express.Router()



function error404(req, res, next)
{
	let error = new Error(),
		locals = {
			title : 'Error 404',
			description : 'Recurso No Encontrado',
			error : error
		}

	error.status = 404

	res.render('error', locals)

	next()
}


router
	.use(movies)
	.get('/', (req, res, next) => {
		//res.end('<h1>Terminamos la configuraci&oacute;n de nuestra primer App en Express</h1>')
		req.getConnection((err, movies) => {
			movies.query('SELECT * FROM movies', (err, rows) => {
				let locals = {
					title : 'Lista de Peliculas',
					data : rows
				}
				res.render('index', locals)
			})
		})
		//next()
	})
	.get('/agregar', (req, res, next) => {
		res.render('add-movie', { title : 'Agregar Pelicula'})
	})
	.post('/', (req, res, next) =>{
		req.getConnection((err, movies) =>{
			//RECOJEMOS DATOS DEL FORMULARIO
			let movie = {
				movie_id : req.body.movie_id,
				name_title : req.body.name_title,
				release_year : req.body.release_year,
				rating : req.body.rating,
				image : req.body.image
			}
			console.log(movie);
			/*(movie_id, title, release_year, rating, image) VALUES*/ 
			movies.query('INSERT INTO movies SET ? ', movie, (err, rows) =>{
				return (err) ? next( new Error('Error al Insertar') ) : res.redirect('/')
			})
		})
	})
	.get('/editar/:movie_id', (req, res, next) => {
		let movie_id = req.params.movie_id
		console.log(movie_id)

		req.getConnection((err, movies) => {
			movies.query('SELECT * FROM movies WHERE movie_id = ?', movie_id, (err, rows) => {
				console.log(err, '---', rows)
				if(err)
				{
					next( new Error('Registro No Encontrado') )
				}
				else
				{
					let locals = {
						title : 'Editar Película',
						data : rows
					}

					res.render('edit-movie', locals)
				}
			})
		})
	})
	.post('/actualizar/:movie_id', (req, res, next) => {
		req.getConnection((err, movies) => {
			let movie = {
				movie_id : req.body.movie_id,
				name_title : req.body.name_title,
				release_year : req.body.release_year,
				rating : req.body.rating,
				image : req.body.image
			}

			console.log(movie)

			movies.query('UPDATE movies SET ? WHERE movie_id = ?', [movie, movie.movie_id], (err, rows) => {
				//return (err) ? res.redirect('/editar/:movie_id') : res.redirect('/')
				return (err) ? next( new Error('Error al actualizar') ) : res.redirect('/')
			})
		})
	})
	.post('/eliminar/:movie_id', (req, res, next) => {
		let movie_id = req.params.movie_id
		console.log(movie_id)

		req.getConnection((err, movies) => {
			movies.query('DELETE FROM movies WHERE movie_id = ?', movie_id, (err, rows) => {
				console.log(err, '---', rows)
				return (err) ? next( new Error('Registro No Encontrado') ) : res.redirect('/')
			})
		})
	})
	.use(error404)
	
module.exports = router