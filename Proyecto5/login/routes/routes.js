var express = require('express');
var router = express.Router();
var controllers = require('.././controllers');
var passport = require('passport')
router.get('/', controllers.HomeController.index);
/* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index', { title: 'Express' });
// });

//RUTAS DE USUARIO
router.get('/auth/signup', controllers.UserController.getSignUp);
router.post('/auth/signup', controllers.UserController.postSignUp);
router.get('/auth/signin', controllers.UserController.getSignIn);
router.post('/auth/signin', passport.authenticate('local', {
    successRedirect: '/', // redirect to the secure profile section
    failureRedirect: '/auth/signin', // redirect back to the signup page if there is an error
    failureFlash: true // allow flash messages
}));

module.exports = router;
