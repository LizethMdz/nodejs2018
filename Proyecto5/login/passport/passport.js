
var mysql = require('mysql');
var bcrypt = require('bcryptjs');
var LocalStrategy = require('passport-local').Strategy;

module.exports = function(passport){
    passport.serializeUser(function(user, done){
        done(null, user);
    });

    passport.deserializeUser(function(obj, done){
        done(null, obj);
    });

    passport.use(new LocalStrategy({
        passReqToCallback : true
    }, function(req, username, password, done){
        var cofig = require('.././database/config');
        var db = mysql.createConnection(cofig);
        db.connect();
        
        db.query('SELECT * FROM employees WHERE name = ?', [username], function(err, rows){
            if(err) throw err;
            db.end();

            if(rows.length > 0){
                var user = rows[0];
                if(bcrypt.compareSync(password, user.password)){
                    return done(null, {
                        id: user.id,
                        name: user.name,
                        email: user.email
                    });
                    
                    //return done(null, false, req.flash('loginMessage', 'No user found.')); // req.flash is the way to set flashdata using connect-flash   
                }
            }
            return done(null, false, req.flash('authmessage', 'Email o Password incorrecto'));
        });
    }
    
    ));

};