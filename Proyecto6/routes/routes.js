/*Integración de los módulos para la utlización de las librerias
* para el uso de los controladores y el envío de correos, asi como,
los middleware de autenticación
*/
var express = require('express');
var router = express.Router();
var controllers = require('.././controllers');
var passport = require('passport')	
var AuthMiddleware = require('.././middleware/auth');
var email = require('nodemailer');

router.get('/', controllers.HomeController.index);
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* Routes de usuario */
router.get('/auth/signup', controllers.UserController.getSignUp);
	
router.post('/auth/signup', controllers.UserController.postSignUp);
 	
router.get('/auth/signin', controllers.UserController.getSignIn);

router.post('/auth/signin', passport.authenticate('local',{
  successRedirect: '/users/panel',
  failureRedirect:'/auth/signin',
  failureFlash: true
}));

router.get('/auth/logout', controllers.UserController.logout);

router.get('/users/panel', AuthMiddleware.isLogged, controllers.UserController.getUserPanel);

/** SESIONES **/
router.get('/users/home', AuthMiddleware.isLogged, controllers.UserController.getPrincipal);

router.get('/users/consulta', AuthMiddleware.isLogged, controllers.UserController.getConsulta);

router.get('/add/sesiones/:idDeudor', AuthMiddleware.isLogged, controllers.UserController.getSesion);

router.get('/users/panel2', AuthMiddleware.isLogged, controllers.UserController.getPrueba);

router.get('/users/editar',  AuthMiddleware.isLogged, controllers.UserController.getUserPanel);

router.post('/users/consulta', controllers.UserController.postSesion);


/** PROCESAR REPORTES */

router.get('/users/reportesMes', AuthMiddleware.isLogged, controllers.UserController.getMes);

router.get('/users/reportesDia', AuthMiddleware.isLogged, controllers.UserController.getDia);

/**  VISTAS AL PROCESAR CLIENTES**/

router.get('/users/eliminarNum/:idDeudor', AuthMiddleware.isLogged, controllers.UserController.getEliminarNum);

router.get('/users/actualizarNum/:idDeudor', AuthMiddleware.isLogged, controllers.UserController.getActualizarNum);

router.get('/users/generarPagos/:idDeudor', AuthMiddleware.isLogged, controllers.UserController.getGenerarPlan);

router.get('/users/enviarCorreo/:idDeudor', AuthMiddleware.isLogged, controllers.UserController.getCorreo);


/** RUTAS PARA PROCESAR A CLIENTES, DESPUES DE UNA LLAMADA**/

router.post('/users/home', controllers.UserController.postEliminarNum);

/************************ */

router.post('/users/panel', controllers.UserController.postActualizarNum);

router.post('/users/generarPagos', controllers.UserController.postGenerarPago);

router.post('/users/panel2', controllers.UserController.postEnviarEmail);


router.get('/users/perfil/:idAgente',  AuthMiddleware.isLogged, controllers.UserController.getPerfil);

router.post('/users/editar', controllers.UserController.postPerfil);


/*REPORTES**/

router.get('/users/reportemes/:idAgente',  AuthMiddleware.isLogged, controllers.UserController.getMes);

router.get('/users/reportedia/:idAgente',  AuthMiddleware.isLogged, controllers.UserController.getDia);

router.post('/users/reportesMes', controllers.UserController.postMes);

router.post('/users/reportesDia', controllers.UserController.postDia);

//router.use(controllers.UserController.error404);

module.exports = router;
