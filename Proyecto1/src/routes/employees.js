const express = require('express')
const router = express.Router();

const mysqlConnection = require('../database.js');


/*RUTA QUE TRAE TODOS LOS USUARIOS DENTRO DE */
router.get("/", (req, res) =>{
    mysqlConnection.query('SELECT * FROM employees', (err, rows, fields) =>{
        if(!err){
            res.json(rows);
        }else {
            console.log(err);
        }
    });
});

/**RUTA QUE TRAE DATOS EN ESPECIFICO DE UN EMPLEADO **/

// GET An Employee
router.get('/:id', (req, res) => {
    const { id } = req.params; 
    mysqlConnection.query('SELECT * FROM employees WHERE id = ?', [id], (err, rows, fields) => {
      if (!err) {
        res.json(rows[0]);
      } else {
        console.log(err);
      }
    });
  });


//INSERT EMPLOYEE  

router.post("/", (req, res) =>{
    const { id, name, salary } = req.body;
    const query = `
        
        CALL employeeAddOrEdit(?, ?, ?);

    `;
    mysqlConnection.query(query, [id, name , salary], (err, rows, fields)=> {
        if(!err){
            res.json({ Status: "Empleado guardado" });
        }else{
            console.log(err);
        }
    });
});


router.up('/:id', (req, res) =>{
    const{ name, salary } = req.params;
    const{ id } = req.params;

    const query = 'CALL employeeAddOrEdit(?,?,?)';
    mysqlConnection.query(query, [id, name, salary] , (err, rows, fields) =>{
        if(!err){
            res.json({status : 'Empleado Actualizado'});
        }else{
            console.log(err);
        }
    });
})


module.exports = router;


